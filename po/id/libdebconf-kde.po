# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# wantoyo <wantoyo@yahoo.com>, 2014.
# Wantoyo <wantoyek@gmail.com>, 2017, 2018, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: libdebconf-kde 0001\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-12-11 01:50+0000\n"
"PO-Revision-Date: 2019-01-19 08:57+0700\n"
"Last-Translator: Wantoyo <wantoyek@gmail.com>\n"
"Language-Team: Indonesian <kde-i18n-doc@kde.org>\n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Wantoyo"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "wantoyo@yahoo.com"

#: src/DebconfGui.cpp:139
#, kde-format
msgid "Debconf on %1"
msgstr "Debconf pada %1"

#: src/DebconfGui.cpp:215
#, kde-format
msgid ""
"<b>Not implemented</b>: The input widget for data type '%1' is not "
"implemented. Will use default of '%2'."
msgstr ""
"<b>Tidak dilaksanakan</b>: Input widget untuk jenis data '%1' tidak "
"dilaksanakan. Akan menggunakan baku dari '%2'."

#: tools/main.cpp:82
#, kde-format
msgctxt "@title"
msgid "Debconf KDE"
msgstr "Debconf KDE"

#: tools/main.cpp:84
#, kde-format
msgctxt "@info"
msgid "Debconf frontend based on Qt"
msgstr "Frontend debconf berdasarkan pada Qt"

#: tools/main.cpp:90
#, kde-format
msgctxt "@info:shell"
msgid "Path to where the socket should be created"
msgstr "Alur di mana soket harus diciptakan"

#: tools/main.cpp:91
#, kde-format
msgctxt "@info:shell value name"
msgid "path_to_socket"
msgstr "path_to_socket"

#: tools/main.cpp:95
#, kde-format
msgctxt "@info:shell"
msgid "FIFO file descriptors for communication with Debconf"
msgstr "File deskriptor FIFO untuk komunikasi dengan Debconf"

#: tools/main.cpp:96
#, kde-format
msgctxt "@info:shell value name"
msgid "read_fd,write_fd"
msgstr "read_fd,write_fd"

#~ msgid "Icon"
#~ msgstr "Ikon"

#~ msgid "Yes"
#~ msgstr "Ya"

#~ msgid "No"
#~ msgstr "Tidak"

#~ msgid "Cancel"
#~ msgstr "Batal"

#~ msgid "Back"
#~ msgstr "Kembali"

#~ msgid "Continue"
#~ msgstr "Lanjutkan"
